import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ForntendComponent } from './forntend.component';

describe('ForntendComponent', () => {
  let component: ForntendComponent;
  let fixture: ComponentFixture<ForntendComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ForntendComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ForntendComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
