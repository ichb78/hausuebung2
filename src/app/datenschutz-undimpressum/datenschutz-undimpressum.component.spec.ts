import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DatenschutzUndimpressumComponent } from './datenschutz-undimpressum.component';

describe('DatenschutzUndimpressumComponent', () => {
  let component: DatenschutzUndimpressumComponent;
  let fixture: ComponentFixture<DatenschutzUndimpressumComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DatenschutzUndimpressumComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DatenschutzUndimpressumComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
