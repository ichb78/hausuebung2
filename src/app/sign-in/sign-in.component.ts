import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { GoogleSigninService } from '../google-signin.service';


@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {

  user: gapi.auth2.GoogleUser | undefined
  constructor(private signInService : GoogleSigninService, private ref : ChangeDetectorRef ) {


  }

  ngOnInit(): void {
      this.signInService.observable().subscribe(user => {
        this.user =user
        this.ref.detectChanges()
      })


  }

  signIn() {
    this.signInService.signin()
  }

  signOut () {
    this.signInService.signOut()
  }
}