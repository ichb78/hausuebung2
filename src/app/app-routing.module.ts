import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BackendComponent } from './backend/backend.component';
import { ContactComponent } from './contact/contact.component';
import { DatenschutzUndimpressumComponent } from './datenschutz-undimpressum/datenschutz-undimpressum.component';
import { FeedbackComponent } from './feedback/feedback.component';
import { FooterComponent } from './footer/footer.component';
import { ForntendComponent } from './forntend/forntend.component';
import { MonthlyComponent } from './monthly/monthly.component';
import { NavbarComponent } from './navbar/navbar.component';
import { SignInComponent } from './sign-in/sign-in.component';
import { StartseiteComponent } from './startseite/startseite.component';
import { UeberunsComponent } from './ueberuns/ueberuns.component';
import { YearlyComponent } from './yearly/yearly.component';
import { HomeComponent } from './home/home.component';
import { BookComponent } from './book/book.component';

const routes: Routes = [{ path: 'startseite', component: StartseiteComponent },{ path: 'datenschutzUndimpressum', component: DatenschutzUndimpressumComponent },{ path: 'feedback', component: FeedbackComponent},{ path: 'backend', component: BackendComponent},{ path: 'contact', component: ContactComponent},{ path: 'footer', component: FooterComponent},{ path: 'frontend', component: ForntendComponent},{ path: 'monthly', component: MonthlyComponent},{ path: 'navbar', component: NavbarComponent},{ path: 'signIn', component: SignInComponent},{ path: 'ueberuns', component: UeberunsComponent},{ path: 'yearly', component: YearlyComponent},{ path: 'home', component:HomeComponent},{ path: 'book', component:BookComponent},];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [StartseiteComponent, DatenschutzUndimpressumComponent, FeedbackComponent, BackendComponent,ContactComponent,FooterComponent,ForntendComponent,MonthlyComponent,NavbarComponent,SignInComponent,UeberunsComponent,YearlyComponent,HomeComponent,BookComponent
];
