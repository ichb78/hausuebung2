import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from "@angular/common/http";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { FooterComponent } from './footer/footer.component';
import { StartseiteComponent } from './startseite/startseite.component';
import { ForntendComponent } from './forntend/forntend.component';
import { BackendComponent } from './backend/backend.component';
import { YearlyComponent } from './yearly/yearly.component';
import { MonthlyComponent } from './monthly/monthly.component';
import { SignInComponent } from './sign-in/sign-in.component';
import { FeedbackComponent } from './feedback/feedback.component';
import { UeberunsComponent } from './ueberuns/ueberuns.component';
import { ContactComponent } from './contact/contact.component';
import { DatenschutzUndimpressumComponent } from './datenschutz-undimpressum/datenschutz-undimpressum.component';
import { HomeComponent } from './home/home.component';
import { BookComponent } from './book/book.component';
import { NewComponent } from './new/new.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    FooterComponent,
    StartseiteComponent,
    ForntendComponent,
    BackendComponent,
    YearlyComponent,
    MonthlyComponent,
    SignInComponent,
    FeedbackComponent,
    UeberunsComponent,
    ContactComponent,
    DatenschutzUndimpressumComponent,
    HomeComponent,
    BookComponent,
    NewComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
