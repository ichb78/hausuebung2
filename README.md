# HausUebung2


# For Education 

die website bietet ihre Nutzern kostenlose web-developpent lern videos 
 

## API's

 - [Books API](https://developers.google.com/books/docs/v1/getting_started?csw=1)
 - [Gmail API](https://developers.google.com/identity/sign-in/web/sign-in)

## Frontend :

- HTML 

- Css
 
- Scss

- JavaScript

## framework :
 
- Angular


## Screenshots

![home navbar](https://git.thm.de/ichb78/hausuebung2/-/raw/master/Screenshots/home%20navbar.png)

![home](https://git.thm.de/ichb78/hausuebung2/-/raw/master/Screenshots/home.png)

![find-a-book](https://git.thm.de/ichb78/hausuebung2/-/raw/master/Screenshots/finde-a-book.png)

![find a book search-Api](https://git.thm.de/ichb78/hausuebung2/-/raw/master/Screenshots/finde%20a%20book-search-Api.png)

![frontend](https://git.thm.de/ichb78/hausuebung2/-/raw/master/Screenshots/frontend.png)

![backend](https://git.thm.de/ichb78/hausuebung2/-/raw/master/Screenshots/backend.png)

![monthly pricing](https://git.thm.de/ichb78/hausuebung2/-/raw/master/Screenshots/monthly%20pricing.png)

![yearly pricing](https://git.thm.de/ichb78/hausuebung2/-/raw/master/Screenshots/pricing-yearly.png)

![footer](https://git.thm.de/ichb78/hausuebung2/-/raw/master/Screenshots/footer.png)

![UeberUns](https://git.thm.de/ichb78/hausuebung2/-/raw/master/Screenshots/UeberUns.png)

![feedback](https://git.thm.de/ichb78/hausuebung2/-/raw/master/Screenshots/feedback.png)

![kontakt](https://git.thm.de/ichb78/hausuebung2/-/raw/master/Screenshots/kontakt.png)

